#!/usr/bin/env python
doc= '''
Usage:
     moc [--controller=<ip>] [--port=<port>]

Options:
   --controller=<ip>    Controller address [default: 127.0.0.1].
   --port=<port>        Controller port [default: 6653].
'''
print doc

import os
from functools import partial
from mininet.net import Mininet
from mininet.cli import CLI
from mininet.log import setLogLevel
from mininet.node import Host, OVSKernelSwitch, UserSwitch, Controller
#from mininet.nodelib import LinuxBridge
from mininet.node import RemoteController
from mininet.topo import Topo
from mininet.log import error
from subprocess import call, check_output
from docopt import docopt



class MOCTopo(Topo):
    def __init__(self):
        Topo.__init__(self)
        x1 = self.addSwitch('x1', cls=OVSKernelSwitch, protocols='OpenFlow13')
        c1 = self.addHost('c1', ip="0.0.0.0/0", mac="00:00:00:00:00:01")
        s1 = self.addHost('s1', ip="10.0.2.100/24", mac="00:00:00:00:00:02")
        #p1 = self.addHost('p1', ip="10.0.2.101/24", mac="00:00:00:00:00:03")
        self.addLink(c1, x1)
        self.addLink(s1, x1)
        #self.addLink(p1, x1)


def run(addr, port):
    net = Mininet(topo=MOCTopo(), controller=partial(RemoteController,ip=addr , port=int(port)) )
    c1 = net.get('c1')
    s1 = net.get('s1')
    x1 = net.get('x1')
    c1.cmd("/usr/sbin/sshd -D&")
    s1.cmd("/usr/sbin/sshd -D&")
    s1.cmd("echo dhcp-range=10.0.2.200,10.0.2.210,12h >> /etc/dnsmasq.conf")
    c1.cmd("dhclient {} -nw ".format(c1.defaultIntf().name))
    s1.cmd('dnsmasq -p 0')
    net.start()
    x1.dpctl('add-flow',  'dl_type=0x0800,nw_proto=17,actions=output:1,2,3,controller')
    CLI(net)
    net.stop()

if __name__ == '__main__':
    setLogLevel('debug')
    args = docopt(doc)
    ctladdr = args.get('--controller')
    ctlport = args.get('--port')
    print "Creating controller connection {}:{}".format(ctladdr, ctlport)
    run(ctladdr, ctlport)
    os.system('sudo mn -c')