from setuptools import setup, find_packages

setup(
    name="moc-dhcp",
    version="0.1.0",
    description='Example Mininet App to test DHCP',
    author='Gary Berger',
    author_email='gberger@brocade.com',
    maintainer='gberger@brocade.com',
    url='https://gitlab.com/firstclassfunc/openflow-testing.git',
    include_package_data=True,
    packages=find_packages(),
    install_requires=['docopt'],
    platforms='any',
    license='BSD')
